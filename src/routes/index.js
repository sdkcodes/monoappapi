const express = require("express");
const AccountService = require("../services/account");
const CustomerService = require("../services/customer");

const router = express.Router();
const { body, validationResult } = require('express-validator');
const passport = require("passport");
require("../../config/passport")(passport);

router.get("/customers", passport.authenticate("jwt"), async function(req, res){
    const customerService = new CustomerService();
    const customers = await customerService.index();
    res.json(customers).status(200);
});
router.post("/customers/:id/accounts",
    passport.authenticate("jwt"),
    body("balance").isInt({min: 1000}),
    async function(req, res){
    const accountService = new AccountService();
    let requestInput = req.body;
    requestInput.customer_id = req.params.id;
    const newAccount = await accountService.create(requestInput);

    res.json({
        status: "success",
        message: "Account created successfully",
        data: newAccount
    }).status(200);

});

router.get("/customers/:id/accounts",
    passport.authenticate("jwt", {session: false}),
    async function (req, res) {
    const customerId = req.params.id;
    const customerService = new CustomerService();
    let accounts = await customerService.getAccounts(customerId);
    res.json({
        status: 'success',
        message: 'Accounts retrieved successfully',
        data: accounts
    }).status(200);
});

router.get("/accounts/:id/balance", async function (req, res) {
    const accountId = req.params.id;
    const accountService = new AccountService();
    let balance = await accountService.getBalance(accountId);
    res.json({
        status: 'success',
        message: 'Account balance retrieved',
        data: balance
    });
});

router.post(
    "/accounts/transfer",
    body('sending_id').isInt({min: 10, max: 16}).withMessage("The sending account id must be an integer"),
    body('receiving_id').isInt({min: 10, max: 16}).withMessage("The receiving account id must be an integer"),
    body('amount').isInt({min: 50}),
    async function (req, res){
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            return res.status(400).json({ errors: errors.array() });
        }
        const sendingId = req.body.sending_id;
        const receivingId = req.body.receiving_id;
        const amount = req.body.amount;
        const accountService = new AccountService();
        try {
            await accountService.doTransfer(sendingId, receivingId, amount);
            res.json({
                status: 'success',
                message: "Transfer successful",
                data: []
            });
        } catch (error) {
            res.json({
                status: "error",
                message: error
            }).status(400);
        }
})
router.get("/accounts/:id/transactions", async function (req, res) {
    const accountService = new AccountService();
    
    try {
        const transactions = await accountService.getTransactions(req.params.id);
        res.json({
            status: 'success',
            message: "Transactions retrieved successfully",
            data: transactions
        });
    } catch (error) {
        res.json({
            status: "error",
            message: "There was a problem fetching transactions"
        });
    }

    


});
module.exports = router;
// export router;