const router = require('express').Router();

const user = require("../../models").User
const bcrypt = require('bcryptjs');
// const moment = require('moment');
const passport = require('passport');
const jwt = require('jsonwebtoken');
const secret = require("../../config/jwt").secret

const { check, body, validationResult } = require('express-validator');
router.post('/register',
    body("email").isEmail().withMessage("Incorrect email format"), 
    body("name").isString(),
    check("password").notEmpty().withMessage("Password is required").isLength({min: 6}).withMessage("Password should be at least 6 characters"),
    (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(400).json({ errors: errors.array() });
    }
    
    user.findOne({
        where: {
            email: req.body.email
        }
    }).then(exitingUser => {
        if (exitingUser){
            res.status(400).json({
                status: 'error',
                'message': "A user with that email already exists"
            })
        }else{
            let newUser = {
                name: req.body.name,
                email: req.body.email,
            }
            bcrypt.genSalt(10, (err, salt) => {
                if (err) throw err;
                bcrypt.hash(req.body.password, salt, (err, hash) => {
                    if (err) throw err;
                    newUser.password = hash;
                    user.create(newUser).then(createdUser => {
                        const payload = {
                            id: newUser.id,
                            email: newUser.email
                        }
                        res.json({
                            status: "success",
                            message: "Account created successfully",
                            data: {
                                user: createdUser,
                                token: jwt.sign(payload, secret)
                            }
                        })
                    }).catch(err => {
                        res.status(400).json({
                            status: 'success',
                            message: "An error occured while createing the account"
                        })
                    })
                    
                })
            })
        }
    })
})


router.post('/login', body("email").isEmail().withMessage("Incorrect email format"), check("password").notEmpty(), async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(400).json({ errors: errors.array() });
    }
    const email = req.body.email;
    const password = req.body.password;
    
    user.findOne({
        where: {email: email}        
    }).then(foundUser => {
        if (!foundUser){
            console.log(foundUser)
            return res.status(401).json({
                status: 'error',
                message: 'No account found. The email and password combination you entered is incorrect'
            })
        }
        bcrypt.compare(password, foundUser.password).then(isMatch => {
            if (isMatch){
                const payload = {
                    id: foundUser.id,
                    email: foundUser.email
                }
                const token = jwt.sign(payload, secret);
                res.status(200).json({
                    status: 'success',
                    message: "Authentication successful",
                    data: {
                        user: foundUser,
                        token: token
                    }
                })
            }else{
                res.status(401).json({
                    status: 'error',
                    message: "No account found. The email and password combination you entered is incorrect"
                });
            }
        })
    }).catch(error => {
        console.log(error);
        throw error;
    })
})
module.exports = router;