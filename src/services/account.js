const db = require("../../models");

class AccountService{
    index(customerId){
        return customerId;
    }

    async create(data){
        const nuban = Math.floor(Math.random() * 899999 + 1000000000);
        data.account_number = nuban;
        
        try {
            const account = await db.BankAccount.create(data);
            console.log(account.customer_id);
            return account;
        } catch (error) {
            return error;
        }
        
    }

    async getBalance(accountId){
        
        try{
            const account = await db.BankAccount.findByPk(accountId);
            return account.balance;
        }catch(error){
            return error;
        }
    }

    async doTransfer(sendingNuban, receivingNuban, amount){
        if (sendingNuban == receivingNuban){
            throw "You can not send to the same account";
        }
        const sendingAccount = await db.BankAccount.findOne({where: {account_number: sendingAccountId}});
        const receivingAccount = await db.BankAccount.findOne({where: {account_number: receivingAccountId }});
        
        if (receivingAccount == null){
            throw "Invalid recipient";
        }
        if (Number(sendingAccount.balance) < Number(amount)){
            throw "Insufficient balance";
        }

        sendingAccount.balance -= amount;
        receivingAccount.balance += amount;

        await sendingAccount.save()
        await receivingAccount.save();
        const reference = Math.random().toString(36).substring(2, 20);
        await db.Transaction.create({
            account_id: sendingAccount.id,
            reference: reference,
            amount: amount,
            direction: "debit",
            status: "success",
            narration: "Transfer between customers"
        });

        await db.Transaction.create({
            account_id: receivingAccountId,
            reference: reference,
            amount: amount,
            direction: "credit",
            status: "success",
            narration: "Transfer received from " + sendingAccount.name
        });
        return "success";
    }

    async getTransactions(accountId){
        const transactions = await db.Transaction.findAll({
            where: {account_id: accountId}
        });
        console.log(transactions);
        return transactions;
    }
}

module.exports = AccountService;