const db = require("../../models");
class CustomerService{
    async index(){
        const customers = await db.Customer.findAll();
        return customers;
    }

    async create(data){
        const customer = await db.Customer.create(data);
        return customer;
    }

    async getAccounts(id){
        const accounts = await db.BankAccount.findAll({
            where: {customer_id: id}
        })

        return accounts;
    }
}

module.exports = CustomerService;