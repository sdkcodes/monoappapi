
const cors = require("cors");
const dotenv = require("dotenv");
const helmet = require("helmet");
const router = require("./routes");
const authRoutes = require("./routes/auth")
const {Sequelize} = require("sequelize");
const express = require("express");
dotenv.config();

if (!process.env.PORT) {
    process.exit(1);
}

const PORT = parseInt(process.env.PORT, 10);

const app = express();

app.use(helmet());
app.use(cors());
app.use(express.json());

app.use("/api/auth", authRoutes);
app.use("/api", router);
app.get("/", function (req, res) {
    res.send("Ok!").status(200);
});

app.listen(PORT, () => {
    console.log(`Listening on port ${PORT}`);
});
