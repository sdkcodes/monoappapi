'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */

    await queryInterface.bulkInsert('Customers', [
      {
        "id": 1,
        "name": "Abba Kyari",
        created_by: 1,
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        "id": 2,
        "name": "Abiola Ajimobi",
        created_by: 1,
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        "id": 3,
        "name": "Prakhar Singh",
        created_by: 1,
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        "id": 4,
        "name": "Abdul Umar",
        created_by: 1,
        createdAt: new Date(),
        updatedAt: new Date()
      }
    ])
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
    await queryInterface.bulkDelete('Customers', null, {});
  }
};
