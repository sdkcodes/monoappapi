const { Strategy, ExtractJwt } = require('passport-jwt');
const models = require("../models");
const secret = require("./jwt").secret;

const options = {
    jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken("Bearer"),
    secretOrKey: secret
}

module.exports = passport => {
    passport.use(new Strategy(options, (payload, done) => {

        models.User.findOne({
            where: {
                email: payload.email
            }
        }).then(user => {

            if (user) {
                return done(null, user)

            } return done(null, false);
        }).catch(err => {
            console.log(err);
        })
    }))
}