'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('Transactions', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      account_id: {
        type: Sequelize.INTEGER
      },
      reference: {
        type: Sequelize.STRING,
      },
      amount: {
        type: Sequelize.INTEGER
      },
      direction: {
        type: Sequelize.STRING
      },
      initiated_by: {
        type: Sequelize.INTEGER
      },
      status: {
        type: Sequelize.STRING
      },
      narration: {
        type: Sequelize.STRING
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('Transactions');
  }
};