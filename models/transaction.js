'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Transaction extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  Transaction.init({
    account_id: DataTypes.INTEGER,
    reference: DataTypes.STRING,
    amount: DataTypes.INTEGER,
    direction: DataTypes.STRING,
    initiated_by: DataTypes.INTEGER,
    status: DataTypes.STRING,
    narration: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'Transaction',
  });
  return Transaction;
};